﻿using UnityEditor;
using UnityEngine;

public class CopySettingsComponent: EditorWindow
{
    private static GameObject objects;
    private static Component[] parentComponents;
    private static Component[] childComponents;

    private static bool[] isParentComponents;
    private static bool[] isChildComponents;
    private static bool isParent = false;
    private static bool isChild = false;

    private static bool isChildCount = false;
    
    [MenuItem("Copy Components/Copy Settings/Copy Settings")]
    public static void Process()
    {
        if(Selection.gameObjects.Length == 0) Debug.Log($"<color=red><b><size=18>Выделите игровой объект!</size></b></color>");
        else if(Selection.gameObjects.Length > 1) Debug.Log($"<color=red><b><size=18>Выделите один объект!</size></b></color>");
        else
        {
            //Присваевыаем выделенный объект
            objects = Selection.gameObjects[0];
            
            //Берем все компоненты у родетеля и задаем размер массива bool
            parentComponents = objects.transform.GetComponents(typeof(Component));
            isParentComponents = new bool[parentComponents.Length];
            
            if(objects.transform.childCount != 0)
            {
                isChildCount = true;
                //Берем все компоненты у первого ребенка и задаем размер массива bool
                childComponents = objects.transform.GetChild(0).GetComponents(typeof(Component));
                isChildComponents = new bool[childComponents.Length];
            }

            EditorWindow.GetWindow(typeof(CopySettingsComponent));
        }

    }

    void OnGUI () 
    {

        isParent = EditorGUILayout.Toggle("Parent components", isParent);

        if(isChildCount) 
            isChild = EditorGUILayout.Toggle("Child components", isChild);

        if(isParent)
        {
            float originalValue = EditorGUIUtility.labelWidth;

            EditorGUIUtility.labelWidth = 300;

            for(int i = 0; i < parentComponents.Length; i++)
            {
                isParentComponents[i] = EditorGUILayout.Toggle(EditorUtility.InstanceIDToObject(parentComponents[i].GetInstanceID()).ToString(), isParentComponents[i]);
            }
                
            EditorGUIUtility.labelWidth = originalValue;
        }  

        if(isChild)
        {
            float originalValue = EditorGUIUtility.labelWidth;

            EditorGUIUtility.labelWidth = 300;

            for(int i = 0; i < childComponents.Length; i++)
            {
                isChildComponents[i] = EditorGUILayout.Toggle(EditorUtility.InstanceIDToObject(childComponents[i].GetInstanceID()).ToString(), isChildComponents[i]);
            }
                
            EditorGUIUtility.labelWidth = originalValue;
        }  

        if (GUILayout.Button("Save"))
            this.Close();
    }


    [MenuItem("Copy Components/Copy Settings/Add Settings")]
    public static void Proces()
    {

        for( int i = 0; i < Selection.gameObjects.Length; i++)
        {

            if(isParent)
            {
                for(int y = 0; y < parentComponents.Length; y++)
                {
                    //Если пользователь выбрал копировать настройки компонента, то переносим их
                    if(isParentComponents[y])
                        EditorUtility.CopySerialized(parentComponents[y], Selection.gameObjects[i].transform.GetComponent(parentComponents[y].GetType()));
                }
            }
    
            if(isChild && Selection.gameObjects[i].transform.childCount >= 1)
            {
                for(int y = 0; y < childComponents.Length; y++)
                {
                    //Если пользователь выбрал копировать настройки компонента, то переносим их
                    if(isChildComponents[y])
                        EditorUtility.CopySerialized(childComponents[y], Selection.gameObjects[i].transform.GetChild(0).GetComponent(childComponents[y].GetType()));
                }
            }
        }

    }

}
